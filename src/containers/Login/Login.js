import React, { Component } from 'react'
import { connect } from 'react-redux'
import Form from './components/Form'
import { logInAction } from '../../redux/user/UserActions.js'
import { Redirect } from 'react-router-dom'
import {userSelector} from './../../redux/user/userSelectors'

class LogIn extends Component {
	render() {
		const { isLogged, isLoginRequest, loginError } = this.props.user
		return (
			<div>
				{ isLogged ? <Redirect to ='/' /> : null}
				<Form logInAction = {this.props.logIn} loginError = {loginError} isLoginRequest = {isLoginRequest} />
			</div>
		)
	}
}

const mapStateToProps = (store) => {
	return {
		user: userSelector(store),
	}
}

const mapDispatchToProps = dispatch =>{
	return {
		logIn: (url, credentials) => dispatch(logInAction(url, credentials)),
	}
}

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(LogIn)
