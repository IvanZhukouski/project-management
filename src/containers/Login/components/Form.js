import React, { Component } from 'react'
import style from './Form.module.scss'
import PropTypes from 'prop-types'

export default class Form extends Component {
	state = {
		emailInvalid: false,
		isValidated: false,
	}
	componentDidMount() {
		this.validateForm()
	}
	formSubmit = (e) => {
		e.preventDefault()
		const pattern = /^[a-z0-9_.-]+@[a-z0-9-]+\.[a-z]{2,6}$/i
		let emailValue = this.refs.userEmail.value
		let passwordValue = this.refs.userPassword.value
		if (emailValue.search(pattern) !== 0) {
			this.setState({ emailInvalid: true })
		} else {
			this.setState({ emailInvalid: false })
			this.props.logInAction({ email: emailValue, password: passwordValue })
		}
	}
	validateForm = () =>{
		let userEmailLength = this.refs.userEmail.value.trim().length
		let userPasswordLength = this.refs.userPassword.value.trim().length

		if (userEmailLength > 0 && userPasswordLength > 0) {
			this.setState({ isValidated: true })
		} else {
			this.setState({ isValidated: false })
		}
	}
	render() {
		const { isLoginRequest, loginError } = this.props
		const emailInvalid = this.state.emailInvalid
		let errClass, errText
		if (emailInvalid) {
			errClass = style.error + ' ' + style.errorVisible
			errText = 'Enter valid email'
		} else if (loginError) {
			errClass = style.error + ' ' + style.errorVisible + ' ' + style.errorRedBg
			errText = 'Email or login is incorrect'
		} else {
			errClass = style.error
		}

		return (
			<div className = {style.log_in}>
				<div className='container'>
					<form className={style.form} onSubmit={this.formSubmit}>
						<img className = {style.image} src='../logo-white.png' alt=''/>

						<div className = {errClass} >{errText}</div>
						<div className={`${style.input_wrapper} form-group`}>
							<div className={ emailInvalid ? style.errorInputWrapper : null} >
								<input
									type='text'
									className = {`${style.input} form-control`}
									onChange = {this.validateForm}
									placeholder='Email' ref='userEmail'
									defaultValue='i.zhukouski@mail.ru'/>
							</div>
							<input type='password' className = {`${style.input} form-control`} onChange = {this.validateForm} placeholder='Password' defaultValue='123' ref='userPassword'/>
							<button type='submit' className = {this.state.isValidated ? `${style.btn} btn btn-primary ` : `${style.btn} ${style.disabled} btn btn-primary disabled` }>Log in</button>
							<div style = {{ backgroundImage: 'url(\'../spinner.gif\')' }} className = { isLoginRequest ? `${style.spinner}` : `${style.spinner} ${style.spinnerInvisible} ` } />
						</div>

					</form>
				</div>
			</div>
		)
	}
}

Form.propTypes = {
	isLoginRequest: PropTypes.bool.isRequired,
	loginError: PropTypes.bool.isRequired,
 	logInAction: PropTypes.func.isRequired,
}
