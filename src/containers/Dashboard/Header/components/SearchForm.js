import React, { useRef } from 'react'
import style from '../../Dashboard.module.scss'

export function SearchForm() {
	const searchVal = useRef(0)
	const onChange = (e)=>{
	}
	return (
		<div>
			<input onChange={onChange} ref={searchVal} type='text' style = {{ backgroundImage: 'url(\'../search.png\')' }} className={style.searcForm} placeholder='Поиск задач'/>
		</div>
	)
}
