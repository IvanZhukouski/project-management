import React, { useState } from 'react'
import style from '../../Dashboard.module.scss'
import { LogOut } from './LogOut'

export function UserData(props) {
	const [menuOpen, setMenuopen] = useState(false)
	const initiales = props.data.firstName.charAt(0) + props.data.lastName.charAt(0)
	const name = props.data.firstName + ' ' + props.data.lastName

	return (
		<div className={style.userInfo}>
			<div className={style.userInitiales}>{initiales}</div>
			<div className={style.userName} onClick={()=> setMenuopen(!menuOpen)}>{props.data.firstName}</div>
			{menuOpen ? <UserMenu name={name} initiales={initiales} logOut={props.logOut} /> : null}
		</div>
	)
}
function UserMenu(props) {
	return (
		<div className={style.userMenu}>
			<div className={style.userInfo}>
				<div className={style.userInitiales}>{props.initiales}</div>
				<p>{props.name}</p>
			</div>
			<LogOut logOutAction={props.logOut}/>
		</div>
	)
}
