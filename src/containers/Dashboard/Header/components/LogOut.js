import React from 'react'
import style from '../../Dashboard.module.scss'

export function LogOut(props) {
	const { logOutAction } = props
	const logOut = function () {
		logOutAction()
		localStorage.removeItem('loggedIn')
		localStorage.removeItem('userEmail')
	}
	return (
		<div className={style.btn_wrapper}>
			<button onClick={logOut} className={style.btn + ' btn'}>Выйти</button>
		</div>
	)
}
