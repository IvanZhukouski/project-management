import React, { Component } from 'react'
import { connect } from 'react-redux'
import { logOutAction, getUserDataAction } from '../../../redux/user/UserActions.js'
import { SearchForm } from './components/SearchForm'
import { UserData } from './components/UserData'
import { userSelector } from '../../../redux/user/userSelectors'

class Header extends Component {
	componentDidMount() {
		const { getUserData } = this.props
		const { userEmail } = this.props.user
		if (userEmail) {
			getUserData(userEmail)
		}
	}
	render() {
		const { isUserDataFetching, isUserDataFetchingError, data } = this.props.user
		const { logOut } = this.props
		return (
			<div className='DashBoardHeader'>
				<SearchForm/>
				<div className='useInfo'>
					{isUserDataFetching ? <div>Loading...</div> : null}
					{isUserDataFetchingError ? <div>Data error, try later</div> : null}
					{data ? <UserData data={data} logOut={logOut} /> : null}
				</div>
			</div>
		)
	}
}


const mapStateToProps = (store) => {
	return {
		user: userSelector(store),
	}
}

const mapDispatchToProps = dispatch =>{
	return {
		logOut: () => dispatch(logOutAction()),
		getUserData: (userEmail) => dispatch(getUserDataAction(userEmail)),
	}
}

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(Header)
