import React, { useState } from 'react'
import style from '../../Dashboard.module.scss'

export function ProjectsPanelHeader() {
	return (
		<div className={style.projectsPanelHeader}>
			<p className={style.panelTitle}>Проекты</p>
			<PanelViewMenu />
		</div>
	)
}

function PanelViewMenu() {
	return (
		<ul className={style.panelView_menu}>
			<ViewMenuItem text={ 'Список' } />
			<ViewMenuItem text={ 'Таблица' } />
			<ViewMenuItem text={ 'Временная шкала' } />
		</ul>
	)
}

function ViewMenuItem(props) {
	return (
		<li className={style.panelView_item}>{props.text}</li>
	)
}
