import React, { Component } from 'react'
import { connect } from 'react-redux'
import { getProjectsDataAction } from '../../../redux/dashboard/DashboardActions'
import { ProjectsPanelHeader } from './components/ProjectsPanelHeader'

export default class ProjectsPanel extends Component {
	render() {
		return (
			<div className='projectsPanel'>
				<ProjectsPanelHeader />
			</div>
		)
	}
}
