import React from 'react'
import Header from './Header/Header'
import Sidebar from './Sidebar/Sidebar'
import ProjectsPanel from './ProjectsPanel/ProjectsPanel'

export default function Dashboard() {
	return (
		<div className='dashboard'>
			<Header/>
			<div className='projects'>
				<Sidebar/>
				<ProjectsPanel />
			</div>
		</div>
	)
}
