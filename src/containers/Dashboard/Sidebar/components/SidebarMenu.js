import React, { useState, useRef } from 'react'
import style from '../../Dashboard.module.scss'

export function SidebarMenu(props) {
	const [ListOpen, setListOpen] = useState()
	const openSearhForm = function () {
		setListOpen(!ListOpen)
		props.onChange({ target: { value: '' } })
	}
	return (
		<div className={style.sidebarMenu}>
			<p className={style.sidebarMenuItem}>МОЯ РАБОТА</p>
			<SidebarMenuItem
				className={style.sidebarMenuItem + ' ' + style.sidebarMenuItem_plus}
				open={ListOpen}
				onClick={openSearhForm}
				onChange={props.onChange}
				list={props.list}>
				ПРОЕКТЫ
			</SidebarMenuItem>
		</div>
	)
}
function SidebarMenuItem(props) {
	const { open, list } = props
	return (
		<div>
			<p className={props.className} onClick={props.onClick}>{props.children}</p>
			{ open ?
				<div>
					<ProjectsSearch onChange={props.onChange} />
					<ul className={style.projectsList}>
						<ProjectsList data={list} />
					</ul>
				</div>
				: null}
		</div>
	)
}
function ProjectsSearch(props) {
	const searchVal = useRef(0)
	return (
		<input type='text' ref={searchVal} className={style.projectSearcForm} placeholder='Введите название' onChange={props.onChange}/>
	)
}
function ProjectsList(props) {
	const data = props.data
	return (
		data[0] ? data.map(elem => (<li key={elem.data.name}><img src='list.ico' />{elem.data.name}</li>)) : <p>Nothing found</p>
	)
}

