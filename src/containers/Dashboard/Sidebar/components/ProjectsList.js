import React, { useState } from 'react'
import { SidebarMenu } from './SidebarMenu'
import style from '../../Dashboard.module.scss'

export function ProjectsList(props) {
	const { data } = props

	const [projectsList, setProjectsList] = useState(data)

	const filterList = (str) => {
		let filteredList = data.filter(function (elem) {
			return elem.data.name.toLowerCase().search(str.toLowerCase()) !== -1
		})
		setProjectsList(filteredList)
	}

	const onProjectsSearchChange = (e) => {
		filterList(e.target.value)
	}

	return (
		<div>
			<SidebarMenu onChange={onProjectsSearchChange} list={projectsList}/>
		</div>
	)
}
