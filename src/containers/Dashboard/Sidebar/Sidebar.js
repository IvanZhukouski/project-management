import React, { Component } from 'react'
import { connect } from 'react-redux'
import { getProjectsDataAction } from '../../../redux/dashboard/DashboardActions.js'
import { ProjectsList } from './components/ProjectsList'
import { dashboardSelector } from '../../../redux/dashboard/dashboardSelectors'

class Sidebar extends Component {
	componentDidMount() {
		const userEmail = localStorage.getItem('userEmail')
		this.props.getProjectsData(userEmail)
	}

	render() {
		const { projectData, isProjectsDataFetching } = this.props.projects
		return (
			<div className='sidebar'>
				{projectData ? <ProjectsList data={projectData} isProjectsDataFetching={isProjectsDataFetching} /> : <p>Loading...</p>}
			</div>
		)
	}
}

const mapStateToProps = (store) => {
	return {
		projects: dashboardSelector(store),
	}
}

const mapDispatchToProps = dispatch =>{
	return {
		getProjectsData: (userEmail) => dispatch(getProjectsDataAction(userEmail)),
	}
}

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(Sidebar)
