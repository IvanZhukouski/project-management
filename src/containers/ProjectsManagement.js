import React, { Component } from 'react'
import { connect } from 'react-redux'
import LogIn from './Login/Login'
import Dasboard from './Dashboard/Dashboard'
import {
	Route,
	withRouter,
	Redirect
} from 'react-router-dom'

class ProjectsManagement extends Component {
	render() {
	  	console.log('ProjectsManagement root render')
	    const { isLogged } = this.props.user
	    return (
			<div className='user'>
				{!isLogged ? <Redirect to='/login'/> : null}
				<Route path = '/' exact component={Dasboard}/>
				<Route path = '/login' component={LogIn}/>
			</div>
		)
	}
}

const mapStateToProps = (store) =>{
	return {
		user: store.user,
	}
}

export default withRouter(connect(
	mapStateToProps
)(ProjectsManagement))
