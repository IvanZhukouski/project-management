import { combineReducers } from 'redux'
import { userReducer } from './user/userReducer'
import { projectsReducer } from './dashboard/projectsReducer'


export const rootReducer = combineReducers({
	user: userReducer,
	projects: projectsReducer,
})
