import * as types from './userConstants'

const initialState = {
	isLogged: localStorage.getItem('loggedIn'),
	loginError: false,
	isLoginRequest: false,
	userEmail: localStorage.getItem('userEmail'),
	isUserDataFetching: false,
	isProjectDataFetching: false,
}

export function userReducer(state = initialState, action) {
	switch (action.type) {
	case types.LOGIN_REQUEST:
		return { ...state, isLoginRequest: true, loginError: false }
	case types.LOGIN_SUCCESS:
		return { ...state, isLoginRequest: false, isLogged: true, userEmail: action.payload.data.email }
	case types.LOGIN_ERROR:
		return { ...state, isLoginRequest: false, isLogged: false, loginError: true }
	case types.LOGOUT_SUCCESS:
		return { ...state, isLogged: false, data: null }
	case types.LOGIN_REQUEST_ERROR:
		return { ...state, isLogged: false, data: null, isLoginRequest: false, requestError: action.payload.data, loginError: true }
	case types.USER_DATA_REQUEST:
		return { ...state, isUserDataFetching: true }
	case types.USER_DATA_REQUEST_SUCCESS:
		return { ...state, isUserDataFetching: false, data: action.payload.personal }
	case types.USER_DATA_REQUEST_ERROR:
		return { ...state, isUserDataFetching: false, isUserDataFetchingError: true }

	default:
		return state
	}
}
