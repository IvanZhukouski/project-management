import * as types from './userConstants'
import { API_URL } from './userConstants.js'

export function logInAction(credentials) {
	let { email, password } = credentials

	function userLogin(url, email) {
		// console.log(url+email);
		return fetch(url + email, {
			method: 'GET',
		})
	}
	return dispatch => {
		dispatch({
			type: types.LOGIN_REQUEST,
		})

		userLogin((API_URL + 'users/'), email)

	    	.then(response => {
		        if (!response.ok) {
		        	dispatch({
  						type: types.LOGIN_REQUEST_ERROR,
  						payload: response.status,
  					})
		        	throw Error(response.statusText)
		        }
	      		return response.json()
		    })
		    .then(response => {
		    	if (response.status === 'ok') {
					setTimeout(function () {
						if (response.data.password === password) {
							localStorage.setItem('loggedIn', true)
							localStorage.setItem('userEmail', email)
							dispatch({
			      						type: types.LOGIN_SUCCESS,
			      						payload: response,
			      					}
			      			)
			      		} else {
			      			dispatch({
			      						type: types.LOGIN_ERROR,
			      						payload: response,
			      					}
	      					)
			      		}
					}, 2000)

	      			return response.data.id
	      		}
	      			dispatch({
	      						type: types.LOGIN_ERROR,
	      						payload: response,
	      					}
	      			)
	      	})


	    .catch(error => {
	    			dispatch({
  						type: types.LOGIN_REQUEST_ERROR,
  						payload: error,
  					})
			})
	}
}

export function getUserDataAction(userEmail) {
	function userData(API_URL, userEmail) {
		// console.log(API_URL+'users/'+ userEmail);
		return fetch(API_URL + 'users/' + userEmail + '/userData', {
			method: 'GET',
		})
	}

	return dispatch => {
		dispatch({
			type: types.USER_DATA_REQUEST,
		})

		userData(API_URL, userEmail)

	    	.then(response => {
		        if (!response.ok) {
		        	dispatch({
  						type: types.USER_DATA_REQUEST_ERROR,
  						payload: response.status,
  					})
		        	throw Error(response.statusText)
		        }
	      		return response.json()
		    })
		    .then(response => {
		    	if (response.status === 'ok') {
					// console.log(response)
					dispatch({
	      						type: types.USER_DATA_REQUEST_SUCCESS,
	      						payload: response.data[0],
	      					}
	      			)


	      			return response.data.id
	      		}
	      			dispatch({
	      						type: types.USER_DATA_REQUEST_ERROR,
	      						payload: response,
	      					}
	      			)
	      	})


	    .catch(error => {
	    			dispatch({
  						type: types.USER_DATA_REQUEST_ERROR,
  						payload: error,
  					})
			})
	}
}

export function logOutAction() {
	return dispatch => {
		dispatch({
			type: types.LOGOUT_SUCCESS,
		})
	}
}
