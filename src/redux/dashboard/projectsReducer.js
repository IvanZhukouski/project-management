import * as types from './dashboardConstants'

const initialState = {
	isProjectsDataFetching: false,
}

export function projectsReducer(state = initialState, action) {
	switch (action.type) {
	case types.PROJECTS_DATA_REQUEST:
		return { ...state, isProjectsDataFetching: true }
	case types.PROJECTS_DATA_REQUEST_SUCCESS:
		return { ...state, isProjectsDataFetching: false, projectData: action.payload }
	case types.PROJECTS_DATA_REQUEST_ERROR:
		return { ...state, isProjectsDataFetching: false, isProjectsDataFetchingError: true }

	default:
		return state
	}
}
