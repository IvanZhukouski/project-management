import { createSelector } from 'reselect'

const getDashboard = state => state.projects

export const dashboardSelector = createSelector(
	getDashboard, projects => projects
)
