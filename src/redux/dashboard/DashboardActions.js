import * as types from './dashboardConstants'
import { API_URL } from '../user/userConstants.js'

export function getProjectsDataAction(userEmail) {
	function projectsData(API_URL, userEmail) {
		console.log(API_URL + 'projects?search=' + userEmail)
		return fetch(API_URL + 'projects?search=' + userEmail, {
			method: 'GET',
		})
	}
	return dispatch => {
		dispatch({
			type: types.PROJECTS_DATA_REQUEST,
		})

		projectsData(API_URL, userEmail)

	    	.then(response => {
		        if (!response.ok) {
		        	dispatch({
  						type: types.PROJECTS_DATA_REQUEST_ERROR,
  						payload: response.status,
  					})
		        	throw Error(response.statusText)
		        }
	      		return response.json()
		    })
		    .then(response => {
		    	if (response.status === 'ok') {
					dispatch({
	      						type: types.PROJECTS_DATA_REQUEST_SUCCESS,
	      						payload: response.data,
	      					}
	      			)


	      			return response.data.id
	      		}
	      			dispatch({
	      						type: types.PROJECTS_DATA_REQUEST_ERROR,
	      						payload: response,
	      					}
	      			)
	      	})


	    .catch(error => {
	    			dispatch({
  						type: types.PROJECTS_DATA_REQUEST_ERROR,
  						payload: error,
  					})
			})
	}
}
