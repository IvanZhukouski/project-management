import React from 'react'
import ProjectsManagement from './containers/ProjectsManagement'

import 'bootstrap/dist/css/bootstrap.min.css'
import './App.css'

export default function App() {
	return (
		<ProjectsManagement/>
	)
}
